
        var refresh_period = 300;
        var refresh_period2 = 700;
        var refresh_period_secs = refresh_period / 1000;
        var current_time = 0.0;
        var data_url = 'http://127.0.0.1:5000/';
        var stopped = false;
        var prevWlk = 0;
        var prevLoad = 0;
        var chartBackgndCol = '#F7F6FB'
        


        var current_values = new Vue({
            el: '#current-values',
            data: {
                Timestamp: 0,
                Load: 0,
                Load2: 0,
                Accel_x: 0,
                Accel_y: 0,
                Accel_z: 0,
                Gyro_x: 0,
                Gyro_y: 0,
                Gyro_z: 0,
                Walking_indicator: 0,
                Walking_frequency: 0, 
                Step_duration: 0,
                Load_mean: 0,
                Unstep_duration: 0,
            },
            mounted: function () {
                var self = this;
                setInterval(function () {
                    $.ajax({
                        url: data_url,
                        method: 'get',
                        dataType: 'json',
                        success: function (data) {
                            if (!stopped) {
                                self.Load = Math.round(data.Load);//data.Load;
                                self.Load2 = Math.round(data.Load2);//data.Load_2;
                                self.Accel_x = data.Accel_x;
                                self.Accel_y = data.Accel_y;
                                self.Accel_z = data.Accel_z;
                                self.Walking_indicator = Math.round(data.Walking_indicator);
                                self.Walking_frequency = Math.round(data.Walking_frequency);
                                self.Step_duration = Math.round(data.Step_duration);
                                //self.Load_mean = Math.round(data.Load_mean);
                                self.Unstep_duration = Math.round(data.Unstep_duration);
                            }
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
                }, refresh_period);
            }
        })

        Highcharts.setOptions({
            global: {
                useUTC: false
            }
        });
        
    var gaugeOptions = {
        chart: {
            type: 'solidgauge',
            backgroundColor: chartBackgndCol,
//            backgroundColor: '#f7f7fa'
            /*background: rgba(10, 10, 10, 0.10); */
        },

        title: null,

        pane: {
            center: ['50%', '50%'],
            size: '100%',
            startAngle: -180,
            endAngle: 180,
            background: {
                backgroundColor:
                    Highcharts.defaultOptions.legend.backgroundColor || '#EEE',
                innerRadius: '60%',
                outerRadius: '100%',
                shape: 'arc'
            }
        },

        exporting: {
            enabled: false
        },

        tooltip: {
            enabled: false
        },

        // the value axis
        yAxis: {
            stops: [
                [0.1, '#DF5353'], // red
                [0.5, '#DDDF0D'], // yellow
                [0.9, '#55BF3B'] // green 
            ],
            lineWidth: 0,
            tickWidth: 0,
            minorTickInterval: null,
            tickAmount: 2,
            title: {
                y: -65

            },
            labels: {
                y: 16
            }
        },

        plotOptions: {
            solidgauge: {
                dataLabels: {
                    y: -25,
                    borderWidth: 0,
                    useHTML: true
                }
            }
        }
    };

        var gaugeFreqOptions = {
        chart: {
            type: 'solidgauge'
        },

        title: null,

        pane: {
            center: ['50%', '50%'],
            size: '100%',
            startAngle: -90,
            endAngle: 90,
            background: {
                backgroundColor:
                    Highcharts.defaultOptions.legend.backgroundColor || '#EEE',
                innerRadius: '60%',
                outerRadius: '100%',
                shape: 'arc'
            }
        },

        exporting: {
            enabled: false
        },

        tooltip: {
            enabled: false
        },

        // the value axis
        yAxis: {
            stops: [
                [0.1, '#DF5353'], // red
                [0.5, '#DDDF0D'], // yellow
                [0.9, '#55BF3B'] // green 
            ],
            lineWidth: 0,
            tickWidth: 0,
            minorTickInterval: null,
            tickAmount: 2,
            title: {
                y: -65

            },
            labels: {
                y: 16
            }
        },

        plotOptions: {
            solidgauge: {
                dataLabels: {
                    y: -25,
                    borderWidth: 0,
                    useHTML: true
                }
            }
        }
    };

    // The walking gauge
        var chartwalking = Highcharts.chart('container-walking', Highcharts.merge(gaugeOptions, {
            yAxis: {
                min: 0,
                max: 100,
                title: {
                    text: '<span style="font-size: 20px; color:#205072;"> Capacité de la marche </span> '
                }
            },

            credits: {
                enabled: false
            },

            series: [{
                name: 'Bilan',
                //data: [current_values.Walking_indicator],//[16],
                data: [current_values.Walking_indicator],
                dataLabels: {
                    format:
                        '<div style="text-align:center">' +
                        '<span style="font-size:30px">{y}</span><br/>' +
                        '<span style="font-size:19px;opacity:0.4">%</span>' +
                        '</div>'
                },
                tooltip: {
                    valueSuffix: ' %'
                }
            }],

            chart: {
                animation: false,
                events: {
                    load: function () {
                        // set up the updating of the chart each second
                       // var series = this.series[0];
                        setInterval(function () {
                            if (!stopped) {
                                //old_val = chartwalking.series[0]
                                //chartwalking.series[0].update({ data: [prevWlk*0.0 + 1*current_values.Walking_indicator] });
                                //chartwalking.series[0].points[0].update({ y: [current_values.Walking_indicator] });
                                chartwalking.series[0].points[0].update(current_values.Walking_indicator); //cupdate({ data: [{x:0, y: 888}] });  current_values.Walking_indicator
                                    var point = chartwalking.series[0].points[0].y;
                                    //point.update(current_values.Walking_indicator);
                                    //chartwalking.series[0].points[0].y = 58;
                                prevWlk = current_values.Walking_indicator;
                            }
                        }, 500);
                    }
                }
                        }

            }    

        ));

// The Trelachement gauge
        var chartTrelachement = Highcharts.chart('container-Trelachement', Highcharts.merge(gaugeFreqOptions, {
            yAxis: {
                min: 0,
                max: 10,
                title: {
                    text: '<span style="font-size: 20px; color:#205072;"> Capacité de la marche </span> '
                }
            },

            credits: {
                enabled: false
            },

            series: [{
                name: 'Bilan',
                //data: [current_values.Walking_indicator],//[16],
                data: [current_values.Unstep_duration],
                dataLabels: {
                    format:
                        '<div style="text-align:center">' +
                        '<span style="font-size:30px">{y}</span><br/>' +
                        '<span style="font-size:19px;opacity:0.4">s</span>' +
                        '</div>'
                },
                tooltip: {
                    valueSuffix: ' s'
                }
            }],

            chart: {
                animation: false,
                events: {
                    load: function () {
                        // set up the updating of the chart each second
                       // var series = this.series[0];
                        setInterval(function () {
                            if (!stopped) {
                                //old_val = chartwalking.series[0]
                                //chartwalking.series[0].update({ data: [prevWlk*0.0 + 1*current_values.Walking_indicator] });
                                //chartwalking.series[0].points[0].update({ y: [current_values.Walking_indicator] });
                                chartTrelachement.series[0].points[0].update(current_values.Unstep_duration); //cupdate({ data: [{x:0, y: 888}] });  current_values.Walking_indicator
                                    var point = chartTrelachement.series[0].points[0].y;
                                    //point.update(current_values.Walking_indicator);
                                    //chartwalking.series[0].points[0].y = 58;
                                prevWlk = current_values.Unstep_duration;
                            }
                        }, 500);
                    }
                },
            backgroundColor: chartBackgndCol,
//            backgroundColor: '#f7f7fa'
                        }

            }    

        ));
        // The Tappui gauge
        var chartTappui = Highcharts.chart('container-Tappui', Highcharts.merge(gaugeFreqOptions, {
            yAxis: {
                min: 0,
                max: 10,
                title: {
                    text: '<span style="font-size: 20px"> Capacité de la marche </span> '
                }
            },

            credits: {
                enabled: false
            },

            series: [{
                name: 'Bilan',
                //data: [current_values.step_duration],//[16],
                data: [current_values.Step_duration],
                dataLabels: {
                    format:
                        '<div style="text-align:center">' +
                        '<span style="font-size:30px">{y}</span><br/>' +
                        '<span style="font-size:19px;opacity:0.4">s</span>' +
                        '</div>'
                },
                tooltip: {
                    valueSuffix: ' %'
                }
            }],

            chart: {
                animation: false,
                events: {
                    load: function () {
                        // set up the updating of the chart each second
                       // var series = this.series[0];
                        setInterval(function () {
                            if (!stopped) {
                                //old_val = chartwalking.series[0]
                                //chartwalking.series[0].update({ data: [prevWlk*0.0 + 1*current_values.Walking_indicator] });
                                //chartwalking.series[0].points[0].update({ y: [current_values.Walking_indicator] });
                                chartTappui.series[0].points[0].update(current_values.Step_duration); //cupdate({ data: [{x:0, y: 888}] });  current_values.Walking_indicator
                                    var point = chartTappui.series[0].points[0].y;
                                    //point.update(current_values.Walking_indicator);
                                    //chartwalking.series[0].points[0].y = 58;
                                prevWlk = current_values.Step_duration;
                                          }
                                }, 500);
                            }
                        },
                        backgroundColor: chartBackgndCol,
//                        backgroundColor: '#f7f7fa'
                }

            }    

        ));

        // The Freq gauge
        var chartFreq = Highcharts.chart('container-freq', Highcharts.merge(gaugeFreqOptions, {
            yAxis: {
                min: 0,
                max: 50,
                title: {
                    text: '<span style="font-size: 20px"> Capacité de la marche </span> '
                }
            },

            credits: {
                enabled: false
            },

            series: [{
                name: 'Bilan',
                //data: [current_values.Walking_indicator],//[16],
                data: [current_values.Walking_frequency],
                dataLabels: {
                    format:
                        '<div style="text-align:center">' +
                        '<span style="font-size:30px">{y}</span><br/>' +
                        '<span style="font-size:19px;opacity:0.4">ppm</span>' +
                        '</div>'
                },
                tooltip: {
                    valueSuffix: ' %'
                }
            }],

            chart: {
                animation: false,
                events: {
                    load: function () {
                        // set up the updating of the chart each second
                       // var series = this.series[0];
                        setInterval(function () {
                            if (!stopped) {
                                //old_val = chartwalking.series[0]
                                //chartwalking.series[0].update({ data: [prevWlk*0.0 + 1*current_values.Walking_indicator] });
                                //chartwalking.series[0].points[0].update({ y: [current_values.Walking_indicator] });
                                chartFreq.series[0].points[0].update(current_values.Walking_frequency); //cupdate({ data: [{x:0, y: 888}] });  current_values.Walking_indicator
                                    var point = chartFreq.series[0].points[0].y;
                                    //point.update(current_values.Walking_indicator);
                                    //chartwalking.series[0].points[0].y = 58;
                                prevWlk = current_values.Walking_frequency;
                                        }
                                    }, 500);
                                }
                            },
                        backgroundColor: chartBackgndCol,
//                        backgroundColor: '#f7f7fa'
                    }

            }    

        ));

        var Load_chart = Highcharts.chart('Load-graph', {
            chart: {
                type: 'spline',
//                 animation: {
//                     duration: refresh_period * 2,
//                 }, // don't animate in old IE
                marginRight: 10,
                events: {
                    load: function () {
                        // set up the updating of the chart each second
                        var series = this.series[0];
                        var series2 = this.series[1];
                        setInterval(function () {
                            if (!stopped) {
                                var time = (current_time + refresh_period_secs),
                                    value = current_values.Load;
                                    value2= current_values.Load2;
                                current_time = time;
//                                 if (series.data.length >= 49) {
//                                     series.data[0].remove();
//                                 }
                                series.addPoint([time, value], redraw=false);
                                series2.addPoint([time, value2], true);
                            }
                        }, refresh_period);
                    }
                },
                backgroundColor: chartBackgndCol,
            },
            title: {
                text: '<span style="font-size: 20px; color:#329D9C;""> Charge (kg) </span> ',
            },
            xAxis: {
                min: 0,
               // max: 200,
                 minPadding: 0.2,
        maxPadding: 0.2,
        maxZoom: 60,
                title: {
                    text: 'Time (s)'
                },
                type: 'integer',
                tickPixelInterval: refresh_period,
            },
            yAxis: {
                min: 0,
                //max: 120,
                title: {
                    text: 'Poids (kg)'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            credits: {
                enabled: false
            },
            'tooltip': {
                enabled: true,
                formatter: function () {
                    return '<b>Time: </b>' + Highcharts.numberFormat(this.x, 2) + '<br/>' +
                           '<b>Load: </b>' + Highcharts.numberFormat(this.y, 2)
                }
            },
            'legend': {
                enabled: true
            },
            'exporting': {
                enabled: false
            },
            'plotOptions': {
                'spline': {
                    'marker': {
                        'enabled': false
                    }
                },
            },
            series: [{
                        name: 'Pied gauche',
                        id: 'sd',
                        color: '#5af1b6',
                        data: [0,00]
                    }, 
                    {
                        name: 'Pied droit',
                        id: 'ny',
                        color: '#f17c5a',
                        data: [0, 0]
                    }]
//             series: [{
//                 color: '#fc60de',
//                 name: 'Load',
//                 data: (function () {
//                     // generate an array of random data
//                     var data = [],
//                         time = current_time,
//                         i;
// 
//                     for (i = -100; i <= 0; i += 1) {
//                         current_time = i * refresh_period_secs;
//                         data.push({
//                             x: current_time,
//                             y: 0
//                         });
//                     }
//                     return data;
//                 }())
//             }]
        });


        

        var Accel_x_chart = Highcharts.chart('Accel_x-graph', {
            chart: {
                type: 'spline',
                animation: {
                    duration: refresh_period * 2,
                }, // don't animate in old IE
                marginRight: 10,
                events: {
                    load: function () {
                        // set up the updating of the chart each second
                        var series = this.series[0];
                        setInterval(function () {
                            if (!stopped) {
                                var time = (current_time + refresh_period_secs),
                                    value = current_values.Accel_x;
                                current_time = time;
                                series.addPoint([time, value], true, true);
                            }
                        }, refresh_period);
                    }
                },
                backgroundColor: chartBackgndCol,
            },
            title: {
                text: '<span style="font-size: 20px; color:#329D9C;""> Accel_x </span> ',
            },
            xAxis: {
                min: 0,
                max: 20,
                title: {
                    text: 'Time (s)'
                },
                type: 'integer',
                tickPixelInterval: refresh_period,
            },
            yAxis: {
                title: {
                    text: 'Accel_x (m/s²)'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            credits: {
                enabled: false
            },
            'tooltip': {
                enabled: true,
                formatter: function () {
                    return '<b>Time: </b>' + Highcharts.numberFormat(this.x, 2) + '<br/>' +
                           '<b>Load: </b>' + Highcharts.numberFormat(this.y, 2)
                }
            },
            'legend': {
                enabled: true
            },
            'exporting': {
                enabled: false
            },
            'plotOptions': {
                'spline': {
                    'marker': {
                        'enabled': false
                    }
                },
            },
            series: [{
                name: 'Accel_x',
                data: (function () {
                    // generate an array of random data
                    var data = [],
                        time = current_time,
                        i;

                    for (i = -10; i <= 0; i += 1) {
                        current_time = i * refresh_period_secs;
                        data.push({
                            x: current_time,
                            y: 0
                        });
                    }
                    return data;
                }())
            }]
        });

        var Accel_y_chart = Highcharts.chart('Accel_y-graph', {
            chart: {
                type: 'spline',
                animation: {
                    duration: refresh_period * 2,
                }, // don't animate in old IE
                marginRight: 10,
                events: {
                    load: function () {
                        // set up the updating of the chart each second
                        var series = this.series[0];
                        setInterval(function () {
                            if (!stopped) {
                                var time = (current_time + refresh_period_secs),
                                    value = current_values.Accel_y;
                                current_time = time;
                                series.addPoint([time, value], true, true);
                            }
                        }, refresh_period);
                    }
                }
                ,
                backgroundColor: chartBackgndCol,
            },
            title: {
                text:  '<span style="font-size: 20px; color:#329D9C;""> Accel_y </span> ',
            },
            xAxis: {
                min: 0,
                max: 20,
                title: {
                    text: 'Time (s)'
                },
                type: 'integer',
                tickPixelInterval: refresh_period,
            },
            yAxis: {
                title: {
                    text: 'Accel_y (m/s²)'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            credits: {
                enabled: false
            },

            'tooltip': {
                enabled: true,
                formatter: function () {
                    return '<b>Time: </b>' + Highcharts.numberFormat(this.x, 2) + '<br/>' +
                           '<b>Load: </b>' + Highcharts.numberFormat(this.y, 2)
                }
            },
            'legend': {
                enabled: true
            },
            'exporting': {
                enabled: false
            },
            'plotOptions': {
                'spline': {
                    'marker': {
                        'enabled': false
                    }
                },
            },
            series: [{
                name: 'Accel_y',
                color: '#38bd42',
                data: (function () {
                    // generate an array of random data
                    var data = [],
                        time = current_time,
                        i;

                    for (i = -10; i <= 0; i += 1) {
                        current_time = i * refresh_period_secs;
                        data.push({
                            x: current_time,
                            y: 0
                        });
                    }
                    return data;
                }())
            }]
        });

        var Accel_z_chart = Highcharts.chart('Accel_z-graph', {
            chart: {
                type: 'spline',
                animation: {
                    duration: refresh_period * 2,
                }, // don't animate in old IE
                marginRight: 10,
                events: {
                    load: function () {
                        // set up the updating of the chart each second
                        var series = this.series[0];
                        setInterval(function () {
                            if (!stopped) {
                                var time = (current_time + refresh_period_secs),
                                    value = current_values.Accel_z;
                                current_time = time;
                                series.addPoint([time, value], true, true);
                            }
                        }, refresh_period);
                    }
                },
                backgroundColor: chartBackgndCol,
            },
            title: {
                text: '<span style="font-size: 20px; color:#329D9C;""> Accel_z </span> ',
            },
            xAxis: {
                min: 0,
                max: 20,
                title: {
                    text: 'Time (s)'
                },
                type: 'integer',
                tickPixelInterval: refresh_period,
            },
            yAxis: {
                title: {
                    text: 'Accel_z (m/s²)'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            credits: {
                enabled: false
            },

            'tooltip': {
                enabled: true,
                formatter: function () {
                    return '<b>Time: </b>' + Highcharts.numberFormat(this.x, 2) + '<br/>' +
                           '<b>Accel_z: </b>' + Highcharts.numberFormat(this.y, 2)
                }
            },
            'legend': {
                enabled: true
            },
            'exporting': {
                enabled: false
            },
            'plotOptions': {
                'spline': {
                    'marker': {
                        'enabled': false
                    }
                },
            },
            series: [{
                name: 'Accel_z',
                color: '#bd3847',
                data: (function () {
                    // generate an array of random data
                    var data = [],
                        time = current_time,
                        i;

                    for (i = -10; i <= 0; i += 1) {
                        current_time = i * refresh_period_secs;
                        data.push({
                            x: current_time,
                            y: 0
                        });
                    }
                    return data;
                }())
            }]
        });



var EqIndic = Highcharts.chart('container-stabIndic', {
    chart: {
        type: 'bar',
        height: 260,
        backgroundColor: chartBackgndCol,
//        backgroundColor: '#f7f7fa',
        events: {
                load: function () {
                    // set up the updating of the chart each second
                   // var series = this.series[0];
                    setInterval(function () {
                        if (!stopped) {
                            EqIndic.series[0].points[0].update(current_values.Load); //cupdate({ data: [{x:0, y: 888}] });  current_values.Walking_indicator
                                var point = EqIndic.series[0].data[0].y;
                            EqIndic.series[1].points[0].update(current_values.Load2); //cupdate({ data: [{x:0, y: 888}] });  current_values.Walking_indicator
                                var point = EqIndic.series[1].data[0].y;
                        }
                    }, 500);
                }
            }
                   
    },
    title: {
        text: '<span style="font-size: 20px; color:#329D9C;""> Force d\'appui par côté </span> '
    },
    subtitle: {
        //text: 'Source: <a href="https://en.wikipedia.org/wiki/World_population">Wikipedia.org</a>'
    },
    xAxis: {
        categories: [''],
        title: {
            text: null
        }
    },
    yAxis: {
        min: 0,
        max: 150,
        title: {
            text: 'Force (kg)',
            align: 'high'
        },
        labels: {
            overflow: 'justify'
        }
    },
    tooltip: {
        valueSuffix: ' kg'
    },
    plotOptions: {
        bar: {
            dataLabels: {
                enabled: true
            }
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'top',
        x: -40,
        y: 80,
        floating: true,
        borderWidth: 1,
        backgroundColor: chartBackgndCol, //f7f7fa
                backgroundColor:
            Highcharts.defaultOptions.legend.backgroundColor || chartBackgndCol,
        shadow: true
    },
    credits: {
        enabled: false
    },
    series: [{
        name: 'Béquille droite',
        showInLegend:false,
        color: '#bd3847',
        data: [current_values.Load]
        
    },
    {
        name: 'Béquille gauche',
        showInLegend:false,
        color: '#808080',
        data: [current_values.Load2]
    }]
});
