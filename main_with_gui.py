#!/usr/bin/env python3

import PySimpleGUI as sg
from threading import Thread
import time

import webbrowser

import asyncio

from bleak import BleakClient
from bleak import BleakScanner

from flask import Flask, jsonify
from flask_cors import CORS
from flask import request 

import json

import sys

from datalogging import DataLogging

app = Flask(__name__)
CORS(app)

CHARACTERISTIC_UUID = "0000f00d-0000-1000-8000-00805f9b34fb"

auth_devices = {
                "CC:50:E3:80:1E:CA":'R',
                "B4:E6:2D:97:11:77":'L',
                "80:7D:3A:FD:ED:56":'L',    
                }

datas = {
            'Timestamp': 0,
            'Load': 0,
            'Load2': 0,
            'Accel_x': 0,
            'Accel_y': 0,
            'Accel_z': 0,
            'Gyro_x': 0,
            'Gyro_y': 0,
            'Gyro_z': 0,
            'Walking_indicator': 0,
            'status': -1,
        }

QUIT = False
beqR_connected = False
beqL_connected = False

@app.route("/")
def serve():
    global datas
    return jsonify(datas)

def _start_async():
    loop = asyncio.new_event_loop()
    Thread(target=loop.run_forever).start()
    return loop

# Submits awaitable to the event loop, but *doesn't* wait for it to
# complete. Returns a concurrent.futures.Future which *may* be used to
# wait for and retrieve the result (or exception, if one was raised)
def submit_async(awaitable):
    return asyncio.run_coroutine_threadsafe(awaitable, _loop)

def stop_async():
    _loop.call_soon_threadsafe(_loop.stop)
    
# TODO Duplicate code 
# TODO DUplcated callback 
def callbackL(sender, data):
    global datas
    global beqL_connected
    load = json.loads(data)["load"]
    if load >= 0:
        datas["Load"] = load
    else:
        datas["Load"] = 0
    beqL_connected = True
    
def callbackR(sender, data):
    global datas
    global beqR_connected
    load = json.loads(data)["load"]
    if load >= 0:
        datas["Load2"] = load
    else:
        datas["Load2"] = 0
    beqR_connected = True

        
def dis_callback(client):
    global beqL_connected
    global beqR_connected
    #print(f"Client {client.address} disconnected, :( ")
    try:
        if auth_devices[client.address] == 'R':
            beqR_connected = False
        elif auth_devices[client.address] == 'L':
            beqL_connected = False
        else:
            print("Error unathorized device disconnected !")
    except KeyError:
        print("Error unknwon device disconnected !")
    
    submit_async(scanner.stop())
    time.sleep(1)
    submit_async(scanner.start())

async def reception_callback(client):
    #print(f"Start reception callback for client: {client.address}")
    client = BleakClient(client.address)
    client.set_disconnected_callback(dis_callback)
    try:
        await client.connect()
        print("connected!")
        try:
            if auth_devices[client.address] == 'R':
                await client.start_notify(CHARACTERISTIC_UUID, callbackR)
            elif auth_devices[client.address] == 'L':
                await client.start_notify(CHARACTERISTIC_UUID, callbackL)
            else:
                print("Unconfigured device, please set Right (R) or Left (L)")
        except KeyError:
            print("Unknown device")

        while client.is_connected:
            await asyncio.sleep(1)
         
    except Exception as e:
        print(e)
    finally:
        print(f"Disconnect from {client.address}")
        await client.disconnect()
        print("Disconnected !")
    
    print("Exit reception callback")
     
def detection_callback(device, advertisement_data):
    print(f"Found a device: {device.address}, RSSI: {device.rssi}, : {advertisement_data}")
    if device.address in auth_devices:
        print(f"Device recognized")
        submit_async(reception_callback(device))
    else:
        print("Device is not an authorized Smarche device")

# Base64 Encoded Radio Button Image of unchecked radio button
radio_unchecked = b'iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAYAAADE6YVjAAAEwElEQVR4nI1W3W9URRT/nZm7ZXdpbajdWpCAjcFEqw88+CACrgaBmFBIwI3fPPpPaJYND/wjYsxFYgwP+BV2kY9gNCIJIhEIBZSWLl3aprvde2fOOT7c3W27fNSTTO7cMzO/35wz55wZYAVRVVMuaxCGoV2qD8PQlsvlQFXNShhPAqduYEr0lrrmhmFoVbVbvWzdQxKGoS0UCgwAFy6PvySx27cQRVvY80YGZyHaIKJbPUHqvCF8k3/tlb+61z2RJAzVFgrE5QuX1q9K9x6Oouj9TCazKmUBawiAglkQO0bsPOqNejOw9qsoan62Z8eWfx9FRMsJkgnnfrv6FgXBUWOD4UzAWJsb8L3ZNFlrCQSwZ8TO6excXe/eux/UY0EcuQkXRx/t3fX6qW6iDomqGiKS87///QaM/Q7K6efXD7rBgf5AVcl7hgBQEYgqVAQEgqroZLXmb9yeTLGgKRztHtu5/XQbr0NSVDU4dAhvj703LGouBpaGXhwZ5v6nem0cO2gCB002AxGBiICZwSwIrEVtZpav3LhjneN76YxsvnDq1D0AKJVKYgBg9NgxKpVKIkpH0ulVQyPrBvxTfb02ih2ICESAdp2darJHIkIUx+jrXW03rB30PT09zzTm5UipVJLR0VECAGqb9csfV16oN3H56f60Hd20gZzzRJR4UzvAusySxBoBi8A5DyLolWvjOv1gjldnUqN7duavFYtFYyoVGACIvd2fzWZSw4P9IqKkLfBugu4GKFSSr4hSbqBfMplMaiFyBwAgn88bU60eUwCI43hbYIBsJk2e+bHAiQVL/xWiSTB4ZmQzabKG4B1vBYBqtapBoVBgVaUfz13aaI3CEBGzgAjouEuXg3bARSG6pImADJEhwLN/TlWJiDhoecOqSHYpUIJPHYclY4CqdBElZ6Otfse9otlKBRaAb5OwqjbaYSnatqKzpEXQAleFsIAlCWERBbfyR4TBwlDVRj4PBgAThqElIgVhPPaicew02R0vi6ClESWcALEkkbV0bhQ7dZ4VpONEpGEYWpPL5QgArLVnYsc0N99QAuC5nWy8JPEYvtW4PS6LfVXFfL2hznkyxv4MALlcjkwlnxcACCj4ul6fjyeqNeOZ1Xu/COoXwX0XkbDAs8B7BjPrVLVm6vVGDOXjAFCpVMSUiCQMQ/vmlpevE+nRyJOZul9jYwix84sEfrG1d94h9A5EQHW6xrEXYwhffFLYe/3dMLSlUkmS2lUsGgB4Nf/OEIleJEPDI88Ocl/vauu8b5UQdA69nS/t2mWIMDM3x+P/TFp2flKM3Tz+569T7dr1UBU+8dPZbWRS30M4s25ojVvT3xcIlNpRpCpd+cI6XZvxd6emUyrUEPW7DhbGzi6twp37mVpu27Nj65lmo7lbgDsT9+dSV2/cotqDWR/HMYt4ERHx7CWKIq7NzPrrN2/TVG0uBcVt56PdBwtjZ1sRKx3sruLaubiOnzy51tq+wy6KP0j19GSsAQwtlnrPjNgxmgvNBWvNl41m8/NPP94/seLN2E0EACd+qGxyse5runi7Zz+iLL2imLcGN1PWnhYNvv3wwM5r3ev+lzzqtdLSB926lV4rK0qxWDTlcvmx7652ZD5J/gNoDCDS80MCGwAAAABJRU5ErkJggg=='

# Base64 Encoded Radio Button Image of checked radio button
radio_checked = b'iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAYAAADE6YVjAAAF40lEQVR4nI2Wf2yWVxXHv+fe+7y/3xbYWvpzhbGRCOkMLoRsjr21A2dI2BalTeaYxsyQ6GT+YTQuQRsy4zRGtmg2gzGNf+jinoK6sY2ZbNK3JQuSuWmiWx3ggBQKfTta+v58nueee/zjfQusMPD88yT3ued87sk593sPcCMTUblDYgZ80R9b90XnDomBiLphjOsEp8WBNQEiohUt2uuLhsji1Ut2zR8Dvq9HBgcZAPqPzK+ZD81DxWpwt2XucYIURCqa6FQmHnuryeBPY31N79dhvkbD77qQAV/0yCBx7tBMV0knn5oPooczyVR8Rcyi0zAS5FBhYDLQ+DDUKJWrtaxRf0hF87uObL3lzIL/J0IWNmx8c7Z/zsR/b7Rp25qex7aOuL09ayhhiECAs4xSyPLBxVD2T4bmQLkZURRNZaLi9nce7P4rfNG4AnQZIqJA5O4Zu5Cbk+TrHVRL/Hi1ie5cnjBgosAyWAAnAnEOEIcYCbRjOXy+an94XHlTHK8tcZUvvP1AR34h3mXIUL1DNm2eaTsXxN5t96R1uNdw15KkrgQMAqAgEAAiAuccnHOI2MFah4wWHJ+t8OMTWp8L9fn2uKwbP9JyHgCwm5wCgIG1IOwmdyH0no4lkq0/uQ22qzmhyzWGIUARINfqEBF4GrBaY83NKb2rJ7Amnlg+U+GnsZvcwNoRqmfSSOu+sYurT1Xdv7a3Oj10R5bKoZAhwAlAtBBTLmViLcMoQhBZfH84j7vXduLhDT3yvX+U5Y8fJXlVMlo7trX7GIZEqdwoFADMMn0pm057X2w3zjkQpH76mFFwTi4BRASWHYxWYCfY+dwb+M3L7+Bn/lHMViN6YDlcOpnwpgO1DQByfVAqXxgRACgHduMKz2JVxlBgHTxNIABnZopIJQwsuwaAYTTBOYcdzx7Ei2MT6O5Yih999bOA1rglAer2IpQZ9wBAvjAiCoODLCJkWXo6TIS4EoqsAwB899dv4q4nfouxf55GNh1HLYhgVD2zHc++jn2HP0D7sjR++c1+3PfpbhSrIZIa1KZCWJYVIkIYHOQF3dFOJJWAA4mAnQOzxdRHRZwtFPGVn76MN94+gZuWphBGFjueOYiR8f+gY1kGzz++CZ+7owuFi5X6nRBBHAxxkhodhQYA04AwQSoVJkTMcE7BMjD8nS0gIuwbn8BjP38Nz+3cjJH8BF7MT6Dz5gye37kJud5OFObKUASwc4gco+o8CFDp6wPXIb6viYhXv3rh5GSkP1UKQ1EaCEJG3NPY++374UTw0lvH8PU9B1GuRWi/KYNffWsz+no7MT1XgSLUa+YcSiHLmcgTD+FJIhL4vla5lgECgFQM4ycDQ8fmI/EgcCKoBhEIgr1PfB4P3nUbpueqaE7HsbeRwfRcGYoEzK7eEMI4XmSZjGKU8PQYAORaBsjkR+EAoNmofadL5d37zrLpbYoktEQeESq1EDFP4xff6Ec26WHL+pVXANAAOITWIUaRvFrQqlyphh0x3g8A+VE4ulIYe18pDLtE+mt72gt2Q0vCzIYCTwHOCYgIqbhBEFlUamG9kA15qVlGRjkcLQR21/kuo2rl4ROPdD+GAV9jZJA/pl259dOtU2LebTW27Zlbq7yyKabnQqnfTAiY619qACzX9SujGP+9GPCTp5bogjXnsiZc996/V0wvaNdVKvyZA2c2zqv0X1pRSz7ZVYnWL9UmFKKABdbVayUigGMYOChn5egM2z3nmr2CJCtZW73/vUd6Dl+twgvWeAfW/fn0vSXd9DttdHe/nsaWFmdXJkEJJUQQROxQDllOlEVeK2gzatvAbE+ng+L29x9dNf7J70nDFupz5/6T7dVY9qli6L6ciMWSXSZAOwWIE6PKhLM2jknroVwNqxmPXlgSXPjB3x9dM7UYcE1IPaPLb/WGA9O3zzM9VAr5XhvZlQ6SIaGSUfRh0jP5ZRS+9Ldt3ccW+/1/JkJYNK0oAg6JmKtmIN+/7rRyYxuqz12LgfD9+tw1dOO563+8H1VJkK2keQAAAABJRU5ErkJggg=='

sg.theme('DarkAmber')   # Add a touch of color
# All the stuff inside your window.
layout = [  #[sg.Text('Some text on Row 1')],
            #[sg.Text('Enter something on Row 2'), sg.InputText()],
            
            #[sg.Radio("Béquille Droite", False, disabled=True, background_color="grey", k="bd")],    
            [[sg.Image(radio_unchecked, enable_events=True, k='-R2-', metadata=False), sg.T('Béquille gauche', enable_events=True, k='-T2-')],[sg.Image(radio_unchecked, enable_events=True, k='-R3-', metadata=False), sg.T('Béquille droite', enable_events=True, k='-T3-')]],
            #[sg.Image(radio_unchecked, enable_events=True, k='-R3-', metadata=False), sg.T('Béquille droite', enable_events=True, k='-T3-')],
            [sg.Text('Poids patient (kg)'), sg.InputText(k="-POIDS-", default_text = "0") ],
            [sg.Text('Poids sur bequille G (kg)'), sg.Text(k="-VALBEQR-") ],
            [sg.Text('Poids sur bequille D (kg)'), sg.Text(k="-VALBEQG-") ],
            [sg.Text('Poids restant (kg)'), sg.Text(k="-TOTAL-") ],
            [sg.Button(button_text = 'Ouvrir les graphiques', k="-GRAPH-"), sg.Button(button_text = 'QUITTER', k="-EXIT-")],
        ]


def gui():
    global QUIT
    # Create the Window
    window = sg.Window('Smarche', layout)
    
    # Event Loop to process "events" and get the "values" of the inputs
    while True:
        event, values = window.read(timeout=500)
        if event == sg.WIN_CLOSED or event == '-EXIT-': # if user closes window or clicks cancel
            stop_async()
            break

        if beqL_connected:
            window['-R2-'].update(source=radio_checked)  
        else:
            window['-R2-'].update(source=radio_unchecked)
        if beqR_connected:
            window['-R3-'].update(source=radio_checked)  
        else:
            window['-R3-'].update(source=radio_unchecked)  
            
        window['-VALBEQG-'].update(value=datas['Load2'])
        window['-VALBEQR-'].update(value=datas['Load'])
        window['-TOTAL-'].update( value=( int(window['-POIDS-'].get()) - ( datas['Load'] + datas['Load2'] )))
        if event == '-GRAPH-':
            webbrowser.open("templates/visualisation.html")
             
    window.close() 
    QUIT = True
    
def web():
    app.run( debug=True, use_reloader=False, host='127.0.0.1', port=5000, threaded=True )
    
def log():
    global datas
    while not QUIT:
        if beqL_connected or beqR_connected:
            datalogger.saveDatas( datas )
            
        time.sleep( 200.0 / 1000.0 )

if __name__ == "__main__":
    gui_th = Thread(target=gui).start()
    _loop = _start_async()
    scanner = BleakScanner()
    scanner.register_detection_callback(detection_callback)
    print("Start Scan ...")
    submit_async(scanner.start())
    #submit_async(scanner.stop())
    # run Flask server
    web_th = Thread(target=web, daemon=True).start()
    datalogger = DataLogging()
    log_th = Thread(target=log).start()
    while True:
        time.sleep(1)
        if QUIT:
            print("Quitting ...")
            sys.exit()

 
