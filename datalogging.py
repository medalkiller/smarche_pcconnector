#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug 20 13:18:01 2021

@author: julien
"""

import time

FILE_BASE_PATH = "data_csv"    
FILE_BASE_NAME = "Smarche_datas"    
FILE_EXTENSION = ".csv"
FILE_ID_NAME = time.strftime("%Y%m%d-%H%M%S")
FILE_NAME = FILE_BASE_PATH + "/" + FILE_BASE_NAME + "_" + FILE_ID_NAME + FILE_EXTENSION
DATAS_HEADER = "Timestamp (ms), Weight Left (kg), Weight Right (kg), "
DATAS_SEP = ","

class DataLogging:
    """A class to log dict Smarche datas into files (csv, etc.) """

    def __init__(self):
        self.s = open(FILE_NAME, 'w')
        self.s.write(DATAS_HEADER)
        self.s.write("\n")
        self.s.close()

    def saveDatas(self, data):
        """Write into csv with given Smarche data as dict """
        self.s = open(FILE_NAME, 'a')
        self.s.truncate()
        try:
            self.s.write(str(data["Timestamp"]) + DATAS_SEP 
                            + str(data["Load"]) + DATAS_SEP
                            + str(data["Load2"]) + DATAS_SEP
                            + "\n")       
        except IndexError:
            pass 
        
        self.s.close()
        
        
