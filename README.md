Tool to communicate with Smarche embedded

# Requirements 

Il nécessite python3 ainsi que les dépdendances listées dans requirements.txt

# Install 

python -m pip install -r requirements.txt

# Use

./main_with_gui.py


# Usage 

- Lancer le logiciel

- allumer 1 ou 2 béquilles

- le logiciel les détecte tout seul au bout de quelques secondes

- les valeurs lues s'affichent dans la fenetre

- on entre le poids du patient

- le poids restant est calculé : ( poids du patient - (poids sur béquille droite + poids sur bequille gauche) )

- le poids sur chaque béquille est affiché sur un graph dans le navigateur en cliquant sur "Ouvrir les graphiques"

- un fichier csv est généré à chaque lancement du logiciel automatiquement dans data_csv/ 
